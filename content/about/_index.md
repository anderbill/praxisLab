---
title: "About"
hero_image: "rodanthePier200801.jpg"
date: "1960-03-01"
nometadata: true
notags: true
noshare: true
nocomments: true
---

<br>
An experimental web and website workbench. There will be trials,
errors, and questions.

Not much more to say about the about-ness of what we are trying to
accomplish here. Nonetheless we are out and about on the
world-wild-web.