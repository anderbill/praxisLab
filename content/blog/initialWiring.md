---
title: "Initial Wiring"
hero_image: "rodanthepier200701-UNF.jpg"
date: 2019-03-18T14:22:54-05:00
description: "Jumping into Hugo and 'whoa; what just happened?'"
categories: ["setup"]
---

## Initial wiring experiences with minimal-academic


Just jumping into Hugo reminds me of how much I already assume I know about websites and how much of that is, well, out of date (to be generous). But this theme's README is pretty good. And the Hugo documentation is also pretty good. By that I mean that even when I do not know what is what I can just type in suggested commands and see what happens.


The primary breakdown right now is figuring out how to get the designated background image to show up on the Home page. I seem to have figured out how to get such a background image on the other pages.

(Um, I did wrap myself completely around an axle by writing and over-writing the base Hugo directory and ended up completely deleting the repository on Gitlab and my dev machine and starting over. Trying to repair the properties of the git repository seemed to be more work than required.)


I still need to set up and document a workflow for using Hugo as a primary blog generator.
