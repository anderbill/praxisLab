---
title: "Minimal Not Simple"
hero_image: "portAransas-201206.jpg"
date: 2019-03-22T10:21:49-05:00
description: "A minimal theme still has parts ...."
categories: ["setup"]
---

## Minimal, Not Simple

When you jump into the Hugo-verse, and you have used various weblog platforms in the past, you might assume that aside from configuration necessities, customization will be straightforward. Or maybe you know better than to assume that, or to assume anything. I would have been happier had I not assumed anything regarding customization.

Of course the only way to learn to use something is to use it. So I now expect a (long?) series of trials with errors.

I think task #1 is to understand the directory structure of a Hugo site and, in particular, the rules or constraints of using the Minimal-Academic theme.
