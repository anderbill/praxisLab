---
title: "The mien of the medium"
hero_image: "hero.jpg"
date: 2018-07-26T17:44:36-07:00
description: "What is it?"
tags: ["mood"]
---

<h2>When we say more we mean more</h2>
More description, more ontological commitment. Let's have the demeanor to mean it.


