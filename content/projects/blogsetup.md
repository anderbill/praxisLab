---
title: "Blog setup - summary"
hero_image: "hero.jpg"
date: 2019-03-21T14:08:59-05:00
---

## Blog setup - the story so far.

BREAKDOWN 1: why is this page not listed under the Projects link?

I was (and still am?) looking for a simple and minimal static weblog
setup. I was attracted to Hugo because of the convenient deployment
service between Gitlab and netlify.com

So the first experiment used the Hyde-Hyde theme and was simple to install. Yay!

The next steps led me into a messy space of inconsistent git
repository states and somewhat frustrating blog rendering trials. My
primary learnings from these experiences include:

*  Git repos are easy to mess up when you make updates from the
command-line as well as the web browser. This was true for me when I
wanted to replace an old Hugo setup with a new one. I took a short cut
and deleted the repo on Gitlab and the directory on my home computer
and started over again. Twice.

*  Hugo can be a bit confusing, but understanding the directory
structure is helpful. This is something I should have done before
embarking on the copy and paste, trial and error method. I am indebted
to [Austin Bohannon](https://github.com/theorangepotato) who helped me understand how to properly deploy the Hugo
Minimal-Academic theme. Two resources that have been informative and
instructive are [ArunRocks Gentle Intro to Hugo](https://arunrocks.com/minimal-hugo-site-tutorial/) and the Hugo [Content Management](https://gohugo.io/content-management/organization/) documentation.

The next experiments will include deciding on header images I like,
and whether each kind of page has a different header image. And
perhaps figuring out how to put a title over the image at the top of
each page. Perhaps.


*  <2019-05-30/31> Summarizing the changes that have been made.

   Reviewing the Hugo documentation and several tutorials on content management and web page layouts resulted in:
   
   + creating a projectspage.html partial to display project posts in a similar manner to blog posts and the homepage.html.

   + this highlighted the overloading of the name "homepage" and will result in some further factoring and refactoring. The upshot is more partials files - a tradeoff to support better naming of webpage parts.

   + removed the Hugo WARN messages on site building using https://vincenttam.gitlab.io/post/2019-04-25-replacing-deprecated-hugo-syntax-in-blog-theme/ as a guide.


